import React from 'react';
import { ButtonWrapper } from './style';

interface ButtonInterface {
  children: React.ReactComponentElement<any> | string;
  color: string;
  onClick?: () => boolean | any;
  size: string;
  icon?: string;
  className?: string;
  style?: object;
  loading?: boolean;
  disabled?: boolean;
}

const Button = ({
  children,
  color,
  onClick,
  size,
  loading = false,
  disabled,
  icon,
  className,
  ...rest
}: ButtonInterface) => {
  return (
    <ButtonWrapper
      disabled={disabled}
      className={`no-select`}
      color={color}
      size={size}
      icon={icon}
    >
      <button
        disabled={disabled}
        onClick={loading ? () => false : onClick}
        className={`button-component ${className}`}
        {...rest}
      >
        {children}
      </button>
    </ButtonWrapper>
  );
};

export default Button;
