import styled from 'styled-components';
import Variables from '../../constants/styleVariables';
// @ts-ignore

// @ts-ignore: Unreachable code error
interface ButtonWrapperInterface {
  disabled?: boolean;
  type?: string;
  size?: string;
  icon?: string;
}
export const ButtonWrapper = styled.div<ButtonWrapperInterface>`
  button.button-component {
    outline: 0;
    border: unset;
    opacity: ${({ disabled }: { disabled?: any }) => (disabled ? '.5' : '1')};
    cursor: ${({ disabled }: { disabled?: any }) =>
      disabled ? 'not-allowed' : 'pointer'};
    text-align: center;
    position: relative;
    ${({ color }: { color?: any }) => {
      switch (color) {
        case 'primary':
          return `
				background: ${Variables.colors.white};
				color: ${Variables.colors.gray};
				&:hover {
					box-shadow: 0px 0px 12px 4px rgba(0, 166, 139, 0.2);
				}
				border: 1px solid ${Variables.colors.white};
			`;
        case 'secondary':
          return `
				background-color: ${Variables.colors.white};
				color: ${Variables.colors.blue};
				border: 1px solid ${Variables.colors.blue};
			`;
        case 'info':
          return `
				background-color: #006cff;
				color: ${Variables.colors.white};
				border: 1px solid #006cff;
			`;
      }
    }};
    ${({ size, icon }: { size?: any; icon?: any }) => {
      switch (size) {
        case 'lg':
          return `
					${icon ? `padding: 8px 23px 8px 47px` : `padding: 8px 23px`};
					font-size: 14px;
					font-weight: ${Variables.fontWeight.bold};
					border-radius: 8px;
					width: max-content;
					height: 54px;
					min-width: max-content;
					min-height: 54px;
				`;
        case 'wide':
          return `
					${icon ? `padding: 8px 23px 8px 47px` : `padding: 8px 23px`};
					font-size: 18px;
					font-weight: ${Variables.fontWeight.bold};
					border-radius: 8px;
					width: 100%;
					height: 54px;
					min-width: max-content;
					min-height: 54px;
				`;
      }
    }};
  }
`;
