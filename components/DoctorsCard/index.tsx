import React from 'react';
import {
  Container,
  CardRow,
  CardRightCell,
  CardMiddleCell,
  CardLeftCell,
  MiddleCellTop,
  MiddleCellBottom,
  Title,
  Description,
  Rate,
  CardLeftTop,
  CardLeftTopTitle,
  CardLeftTopDistance,
  CardLeftMiddle,
  CardLeftBottom,
  CardLeftMiddleRow,
  CardLeftMiddleCellRight,
  CardLeftMiddleCellLeft,
} from './style';
import Avatar from './avatar';
import Button from '../button';
import DefaultImage from '../../assets/avatar/womanLarge.png';
import ManImage from '../../assets/avatar/manLarge.png';
import WomanImage from '../../assets/avatar/womanLarge.png';

interface DocotorCardInterface {
  info?: any;
  loading: boolean;
}

const DoctorsCard = ({ info, loading }: DocotorCardInterface) => {
  // todo: handle pagination
  return (
    <Container>
      <CardRow>
        <CardRightCell>
          <Avatar
            active={info?.Profile?.IsOnline || false}
            src={info?.Profile?.Image ? ManImage : WomanImage || DefaultImage}
          />
        </CardRightCell>
        <CardMiddleCell>
          <MiddleCellTop>
            <Title>{info?.Profile?.FullName || '----'}</Title>
            <Description>{info?.Profile?.FieldName || '----'}</Description>
            <Rate>4.3</Rate>
          </MiddleCellTop>
          <MiddleCellBottom>
            <Description>
              {info?.Profile?.MedicativeComment || '----'}
            </Description>
          </MiddleCellBottom>
        </CardMiddleCell>
        <CardLeftCell>
          <CardLeftTop>
            <CardLeftTopTitle
              title={info?.MedicalCenterList[0]?.AreaName || '----'}
            >
              {info?.MedicalCenterList[0]?.AreaName || '----'}
            </CardLeftTopTitle>
            <CardLeftTopDistance>فاصله تا مطب: 12km</CardLeftTopDistance>
          </CardLeftTop>
          <CardLeftMiddle>
            <CardLeftMiddleRow>
              <CardLeftMiddleCellRight>ویزیت ها</CardLeftMiddleCellRight>
              <CardLeftMiddleCellLeft>حضوری/تصویری/صوتی</CardLeftMiddleCellLeft>
            </CardLeftMiddleRow>
            <CardLeftMiddleRow>
              <CardLeftMiddleCellRight>
                تا اولین نوبت آزاد
              </CardLeftMiddleCellRight>
              <CardLeftMiddleCellLeft>۴ ساعت باقی مانده</CardLeftMiddleCellLeft>
            </CardLeftMiddleRow>
            <CardLeftMiddleRow>
              <CardLeftMiddleCellRight>۵۷ نظر</CardLeftMiddleCellRight>
              <CardLeftMiddleCellLeft>۵۶٪ رضایت</CardLeftMiddleCellLeft>
            </CardLeftMiddleRow>
          </CardLeftMiddle>
          <CardLeftBottom>
            <Button color={'info'} size={'wide'}>
              مشاهده و دریافت نوبت
            </Button>
          </CardLeftBottom>
        </CardLeftCell>
      </CardRow>
    </Container>
  );
};

export default DoctorsCard;
