import React, { ReactComponentElement } from 'react';
import { Container } from './style';
import Image from 'next/image';

const Avatar = ({
  src,
  active = false,
}: {
  src: any;
  active: boolean | undefined;
}) => {
  return (
    <Container active={active}>
      <Image src={src} />
    </Container>
  );
};

export default Avatar;
