import styled from 'styled-components';
import Variables from '../../../constants/styleVariables';

export const Container = styled.div`
  width: 100%;
  min-width: 169px;
  min-height: 169px;
  max-width: 169px;
  max-height: 169px;
  border-radius: 50%;
  margin: 0 auto;
  position: relative;
  &:before {
    position: absolute;
    bottom: 10%;
    right: 9%;
    width: 16px;
    height: 16px;
    border-radius: 50%;
    content: '';
    z-index: 2;
    border: 1px solid ${Variables.colors.white};
    background-color: ${({ active }: { active: boolean | any }) =>
      active ? Variables.colors.green : Variables.colors.coolGrey};
  }
`;
