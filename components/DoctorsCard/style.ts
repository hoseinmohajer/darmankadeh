import styled from 'styled-components';
import Variables from '../../constants/styleVariables';

export const Container = styled.div`
  width: 100%;
  height: 300px;
  border-radius: 4px;
  border: 1px solid #0070f3;
  margin-bottom: 20px;
  display: table;
`;
export const CardRow = styled.div`
  width: 100%;
  display: table-row;
`;
export const CardRightCell = styled.div`
  width: 20%;
  display: table-cell;
  padding: 10px;
  text-align: center;
  margin: 0 auto;
`;

export const CardMiddleCell = styled.div`
  width: 40%;
  display: table-cell;
  vertical-align: top;
  padding-right: 20px;
`;
export const MiddleCellTop = styled.div`
  width: 100%;
  height: 50%;
  display: flex;
  justify-content: space-between;
  align-items: flex-start;
  flex-direction: column;
`;
export const MiddleCellBottom = styled.div`
  width: 100%;
  height: 50%;
  display: flex;
  justify-content: center;
  align-items: flex-start;
  flex-direction: column;
`;
export const Title = styled.div`
  text-align: right;
  color: #011533;
  font-size: 24px;
  width: 100%;
  padding-top: 16px;
`;
export const Description = styled.div`
  text-align: justify;
  padding-left: 8px;
  color: #505762;
  font-size: 14px;
  width: 98%;
`;
export const Rate = styled.div`
  border-radius: 2px;
  background-color: #f1f1f4;
  color: #505762;
  padding: 8px;
  min-width: 68px;
  min-height: 16px;
  text-align: left;
`;

export const CardLeftCell = styled.div`
  width: 40%;
  display: table-cell;
  vertical-align: middle;
  padding-left: 10px;
`;
export const CardLeftTop = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  height: 36px;
`;
export const CardLeftTopTitle = styled.div`
  text-align: right;
  font-size: 22px;
  color: #006cff;
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
  max-width: 185px;
`;
export const CardLeftTopDistance = styled.div`
  background-color: #f1f1f4;
  width: max-content;
  height: 24px;
  border-radius: 3px;
  color: ${Variables.colors.gray};
  padding: 5px 5px;
  text-align: center;
  font-size: 12px;
`;
export const CardLeftMiddle = styled.div`
  width: 100%;
  height: calc(100% - 140px);
  border-radius: 4px;
  background-color: #e5f0ff;
  padding: 8px 10px;
  margin-top: 8px;
  display: table;
`;
export const CardLeftMiddleRow = styled.div`
  width: 100%;
  display: table-row;
  height: 30%;
`;
export const CardLeftMiddleCellRight = styled.div`
  display: table-cell;
  color: #004099;
  font-size: 14px;
  text-align: right;
  vertical-align: middle;
`;
export const CardLeftMiddleCellLeft = styled.div`
  display: table-cell;
  color: #505762;
  font-size: 14px;
  text-align: left;
  vertical-align: middle;
`;
export const CardLeftBottom = styled.div`
  margin-top: 16px;
`;
