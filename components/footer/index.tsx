import React from 'react';
import { Container, Title, Stores, ImageWrapper, MainContainer } from './style';
import PlayStore from '../../assets/applications/play-store.png';
import Bazar from '../../assets/applications/bazar.png';
import Hat from '../../assets/applications/hat.png';
import Image from 'next/image';

const Footer = () => {
  return (
    <MainContainer>
      <Container>
        <Title>همین حالا دانلود کنید!</Title>
        <Stores>
          <ImageWrapper>
            <Image src={PlayStore} />
          </ImageWrapper>
          <ImageWrapper>
            <Image src={Bazar} />
          </ImageWrapper>
          <ImageWrapper>
            <Image src={Hat} />
          </ImageWrapper>
        </Stores>
      </Container>
    </MainContainer>
  );
};

export default Footer;
