import styled from 'styled-components';
import Variables from '../../constants/styleVariables';

export const MainContainer = styled.div`
  width: 100%;
  background-color: ${Variables.colors.darkBlue};
  position: relative;
  overflow: hidden;
  &:before {
    content: '';
    width: 160px;
    height: 160px;
    border-radius: 50%;
    background-color: ${Variables.colors.darkBlue_100};
    position: absolute;
    top: -42px;
    right: -31px;
  }
  &:after {
    content: '';
    width: 86px;
    height: 86px;
    border-radius: 50%;
    background-color: ${Variables.colors.darkBlue_100};
    position: absolute;
    bottom: -36px;
    left: -37px;
  }
`;
export const Container = styled.div`
  background-color: ${Variables.colors.darkBlue};
  width: 100%;
  max-width: 1577px;
  min-height: 145px;
  margin: 0 auto;
`;
export const Title = styled.div`
  font-size: 24px;
  width: 100%;
  margin: 30px auto;
  color: ${Variables.colors.white};
  font-weight: bold;
  text-align: center;
`;
export const Stores = styled.div`
  width: calc(100% - 360px);
  margin: 40px auto;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-wrap: wrap;
`;
export const ImageWrapper = styled.div`
  margin-right: 45px;
  margin-bottom: 16px;
  &:first-child {
    margin-right: 0;
  }
  @media screen and (max-width: 768px) {
    margin-right: 0;
  }
`;
