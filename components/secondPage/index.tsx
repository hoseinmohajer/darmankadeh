import React from 'react';
import {
  MainContainer,
  Container,
  TopSection,
  SecondSection,
  ThirdSection,
  ThirdSectionRight,
  ThirdSectionLeft,
} from './style';
import DoctorsCard from '../DoctorsCard';
import Head from 'next/head';

interface SecondPageInterface {
  data: object | any;
  loading: boolean;
}
const SecondPageComponent = ({ data, loading = true }: SecondPageInterface) => {
  return (
    <>
      <Head>
        <title>{data?.SeoParams?.Title || ''}</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        <meta name="description" content={data?.SeoParams?.HeaderDescription} />
        <meta name="title" content={data?.SeoParams?.HeaderTitle} />
      </Head>
      <MainContainer>
        <Container>
          <TopSection>Todo: should be completed (response)</TopSection>
          <SecondSection>Todo: should be completed (response)</SecondSection>
          <ThirdSection>
            <ThirdSectionRight>
              {data && data.Result && data.Result.MedicativeInfo ? (
                data.Result.MedicativeInfo.map((info, key) => (
                  <DoctorsCard key={key} info={info} loading={loading} />
                ))
              ) : (
                <div>Something went wrong! (cant fetch data)</div>
              )}
            </ThirdSectionRight>
            <ThirdSectionLeft>
              <h1>Map Section</h1>
            </ThirdSectionLeft>
          </ThirdSection>
        </Container>
      </MainContainer>
    </>
  );
};

export default SecondPageComponent;
