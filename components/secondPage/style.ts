import styled from 'styled-components';
import Variables from '../../constants/styleVariables';

export const MainContainer = styled.div`
  width: 100%;
  min-height: 600px;
`;
export const Container = styled.div`
  width: 100%;
  max-width: 1577px;
  margin: 0 auto;
  padding: 0 21px;
  @media screen and (max-width: 768px) {
  }
`;
export const TopSection = styled.div`
  min-height: 256px;
  background-color: #eaeaea;
  padding: 30px;
  margin-bottom: 40px;
`;
export const SecondSection = styled.div`
  min-height: 83px;
  background-color: antiquewhite;
  padding: 30px;
  margin-bottom: 32px;
`;
export const ThirdSection = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: flex-start;
  width: 100%;
`;
export const ThirdSectionRight = styled.div`
  width: 65%;
  padding-left: 5%;
  @media screen and (max-width: 768px) {
    width: 100%;
  }
`;
export const ThirdSectionLeft = styled.div`
  text-align: center;
  width: 30%;
  background-color: darkgray;
  height: 364px;
  @media screen and (max-width: 768px) {
    display: none;
  }
`;
