import React from 'react';
import { Container, ChildrenContainer } from './style';
import Header from '../header';
import Footer from '../footer';

const MainLayout = ({ children }: React.PropsWithChildren<{}>) => {
  return (
    <Container>
      <Header />
      <ChildrenContainer>{children}</ChildrenContainer>
      <Footer />
    </Container>
  );
};

export default MainLayout;
