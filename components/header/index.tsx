import React from 'react';
import {
  Container,
  LogoContainer,
  Profile,
  RightSideContainer,
  LeftSideContainer,
  MainContainer,
} from './style';
import Logo from '../../assets/darmankade_Vector with heart.svg';
import Image from 'next/image';
import Search from './search';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faShoppingCart } from '@fortawesome/free-solid-svg-icons';
import Navbar from './navbar';
import Link from 'next/link';

const Header = () => {
  return (
    <MainContainer>
      <Container>
        <RightSideContainer>
          <Link href={'/'}>
            <a>
              <LogoContainer>
                <Image src={Logo} />
              </LogoContainer>
            </a>
          </Link>
          <Search />
        </RightSideContainer>
        <LeftSideContainer>
          <Profile>ورود</Profile>
          <FontAwesomeIcon className={'shopping-card'} icon={faShoppingCart} />
        </LeftSideContainer>
      </Container>
      <Navbar />
    </MainContainer>
  );
};

export default Header;
