import styled from 'styled-components';
import Variables from '../../constants/styleVariables';

export const MainContainer = styled.div`
  max-width: 1577px;
  width: 100%;
  margin: 0 auto;
  position: relative;
`;
export const Container = styled.div`
  background-color: ${Variables.colors.white};
  min-height: 60px;
  width: 100%;
  padding: 23px 21px 8px 21px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  @media screen and (max-width: 768px) {
    min-height: 40px;
    padding: 5px 10px 15px 10px;
    justify-content: space-between;
  }
`;
export const RightSideContainer = styled.div`
  width: 50%;
  display: flex;
  justify-content: flex-start;
  align-items: center;
  @media screen and (max-width: 768px) {
    justify-content: flex-end;
    width: 55%;
  }
`;
export const LeftSideContainer = styled.div`
  width: 50%;
  display: flex;
  justify-content: flex-end;
  align-items: center;
  .shopping-card {
    color: ${Variables.colors.coolGrey};
  }
  @media screen and (max-width: 768px) {
    width: 45%;
  }
`;
export const LogoContainer = styled.div`
  min-width: 49px;
  min-height: 21px;
`;
export const Profile = styled.div`
  color: ${Variables.colors.black_100};
  width: max-content;
  font-size: 16px;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.5;
  letter-spacing: normal;
  text-align: right;
  height: inherit;
  margin-left: 35px;
  @media screen and (max-width: 768px) {
    display: none;
  }
`;
