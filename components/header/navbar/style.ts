import styled from 'styled-components';
import Variables from '../../../constants/styleVariables';

export const Container = styled.div`
  min-height: 28px;
  width: 100%;
  margin-top: 16px;
  padding: 10px 21px;
  @media screen and (max-width: 768px) {
    width: max-content;
    margin: 0;
    min-height: 0;
    height: 0;
    padding: 0;
  }
`;
export const Ul = styled.ul`
  width: 100%;
  display: flex;
  justify-content: flex-start;
  align-items: center;
  flex-wrap: wrap;
  padding: 0;
  margin: 0;
  //todo: handle small-devices menu list
  @media screen and (max-width: 768px) {
    display: none;
  }
`;
export const Li = styled.li`
  padding: 0;
  margin: 0 1.6% 0 0;
  width: max-content;
  list-style: none;
  a.header-navbar-link {
    display: flex;
    justify-content: center;
    align-items: center;
  }
  &:first-child {
    margin-right: 0;
  }
`;
export const Text = styled.div`
  height: inherit;
  font-size: 14px;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.5;
  letter-spacing: normal;
  text-align: right;
  color: ${Variables.colors.black_100};

  margin-right: 5px;
`;
export const IconContainer = styled.div`
  color: ${Variables.colors.gray};
  display: none;
  @media screen and (max-width: 768px) {
    display: block;
    position: absolute;
    right: 10px;
    top: calc(50% - 15px);
  }
`;
