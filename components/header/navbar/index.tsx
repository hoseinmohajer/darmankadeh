import React from 'react';
import { Container, Ul, Li, Text, IconContainer } from './style';
import Image from 'next/image';
import Link from 'next/link';
import item1 from '../../../assets/navBarIcons/item1.svg';
import item2 from '../../../assets/navBarIcons/item2.svg';
import item3 from '../../../assets/navBarIcons/item3.svg';
import item4 from '../../../assets/navBarIcons/item4.svg';
import item5 from '../../../assets/navBarIcons/item5.svg';
import item6 from '../../../assets/navBarIcons/item6.svg';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBars } from '@fortawesome/free-solid-svg-icons';

const Navbar = () => {
  const Img: any = { item1, item2, item3, item4, item5, item6 };
  return (
    <Container>
      <IconContainer>
        <FontAwesomeIcon icon={faBars} />
      </IconContainer>
      <Ul>
        {Items.map((item, key) => {
          return (
            <Li key={key}>
              <Link href={`${item.path}`}>
                <a className="header-navbar-link">
                  <Image src={Img[item.icon]} />
                  <Text>{item.label}</Text>
                </a>
              </Link>
            </Li>
          );
        })}
      </Ul>
    </Container>
  );
};

export default Navbar;

const Items = [
  {
    icon: 'item1',
    label: 'نوبت حضوری',
    path: '/secondpage',
  },
  {
    icon: 'item2',
    label: 'ویزیت انلاین',
    path: '/thirdpage',
  },
  {
    icon: 'item3',
    label: 'آزمایش در محل',
    path: '/fourthpage',
  },
  {
    icon: 'item4',
    label: 'کرونا',
    path: '/fifthpage',
  },
  {
    icon: 'item5',
    label: 'مجله پزشکی',
    path: '/secondpage',
  },
  {
    icon: 'item6',
    label: 'عضویت سرویس‌دهنده',
    path: '/secondpage',
  },
];
