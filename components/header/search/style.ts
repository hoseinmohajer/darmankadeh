import styled from 'styled-components';
import Variables from '../../../constants/styleVariables';

export const Container = styled.div`
  min-width: 187.5px;
  width: 60%;
  min-height: 19px;
  padding: 4px 8px 4px 30px;
  border-radius: 4px;
  border: solid 1px ${Variables.colors.coolGrey};
  background-color: ${Variables.colors.white};
  position: relative;
  margin-right: 8px;
  margin-top: 12px;
  .search-icon {
    position: absolute;
    left: 8px;
    top: calc(50% - 8px);
    color: ${Variables.colors.coolGrey};
  }
  .search-input {
    border: unset;
    outline: 0;
    height: inherit;
    min-height: 19px;
    width: calc(100% - 30px);
    padding: 0;
    font-size: 14px;
    color: ${Variables.colors.coolGrey};
  }
  @media screen and (max-width: 768px) {
    display: none;
  }
`;
