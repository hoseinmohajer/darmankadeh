import React, { useState } from 'react';
import { Container } from './style';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch } from '@fortawesome/free-solid-svg-icons';

const Search = () => {
  const [value, setValue] = useState('');
  return (
    <Container>
      <input
        className={'search-input'}
        type="text"
        onChange={(e) => setValue(e.target.value)}
        value={value}
        placeholder={'جستجو در درمانکده…'}
      />
      <FontAwesomeIcon className="search-icon" icon={faSearch} />
    </Container>
  );
};

export default Search;
