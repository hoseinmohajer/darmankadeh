import React from 'react';
import {
  MainContainer,
  Container,
  ImageContainer,
  TextContainer,
  SmallTitle,
  Title,
  Text,
  ButtonContainer,
  BottomText,
  SmallDevicesImageContainer,
  TopSection,
  RightSideBox,
  LeftSideBox,
  TopSectionRow,
} from './style';
import Button from '../button';
import cellPhones_small from '../../assets/cellPhones.png';
import cellPhones_large from '../../assets/cellPhones_larg.png';
import Image from 'next/image';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowLeft } from '@fortawesome/free-solid-svg-icons';

const MainPage = () => {
  return (
    <MainContainer>
      <TopSection>
        <TopSectionRow>
          <RightSideBox>
            <TextContainer>
              <div>
                <SmallTitle>
                  <FontAwesomeIcon
                    className={'small-title-icon'}
                    icon={faArrowLeft}
                  />
                  اپلیکیشن درمانکده
                </SmallTitle>
                <Title>سلامتی شما از همه‌چیز مهم‌تر است.</Title>
                <Text>
                  نوبت دهی بهترین پزشکان متخصص و فوق تخصص را در دستان خود داشته
                  باشید و در تمام شبانه روز از آنها، هوشمندانه، سریع و آسان نوبت
                  بگیرید.
                </Text>
              </div>
              <SmallDevicesImageContainer>
                <Image src={cellPhones_small} />
              </SmallDevicesImageContainer>
              <div>
                <ButtonContainer>
                  <Button
                    size={'lg'}
                    color={'primary'}
                    style={{ marginLeft: '20px', marginTop: '20px' }}
                  >
                    برای دریافت لینک دانلود اپ، شماره خود را وارد کنید.
                  </Button>
                  <Button
                    size={'lg'}
                    color={'secondary'}
                    style={{ marginTop: '20px' }}
                  >
                    دریافت لینک دانلود
                  </Button>
                </ButtonContainer>
                <BottomText>
                  با ثبت اولین رزرو از اپلیکیشن درمانکده ۱۵۰۰۰ تومان تخفیف
                  بگیرید.
                </BottomText>
              </div>
            </TextContainer>
          </RightSideBox>
          <LeftSideBox>
            <Image src={cellPhones_large} />
          </LeftSideBox>
        </TopSectionRow>
      </TopSection>
      <div
        style={{
          width: '100%',
          height: '600px',
          border: '1px solid red',
          backgroundColor: '#fafafa',
        }}
      >
        <h2>Some other content will have placed here.</h2>
      </div>
    </MainContainer>
  );
};

export default MainPage;
