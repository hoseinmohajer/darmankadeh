import styled from 'styled-components';
import Variables from '../../constants/styleVariables';

export const MainContainer = styled.div`
  width: 100%;
  min-height: 600px;
`;
export const Container = styled.div`
  width: 100%;
  max-width: 1577px;
  margin: 10px auto 0 auto;
  min-height: calc(100vh - 105px);
  height: calc(100vh - 105px);
  position: relative;
  &:before {
    content: ' ';
    min-width: 430px;
    width: 57%;
    border-radius: 65px 0 0 65px;
    min-height: 500px;
    height: calc(100vh - 200px);
    background: url('./mainPage.png') top right no-repeat;
    position: absolute;
    right: 0;
    top: 0;
    z-index: 1;
  }
  &:after {
    z-index: -1;
    content: ' ';
    min-width: 430px;
    width: 58%;
    border-radius: 65px 0 0 65px;
    min-height: 500px;
    height: calc(100vh - 170px);
    background: url('./mainPageBackground.png') top right no-repeat;
    position: absolute;
    right: 30px;
    top: 20px;
  }
  @media screen and (max-width: 768px) {
    &:before {
      width: 96%;
      min-width: 96%;
    }
    &:after {
      width: calc(95% - 30px);
      min-width: 95%;
    }
  }
`;
export const ImageContainer = styled.div`
  z-index: 2;
  position: absolute;
  left: 0;
  top: 40px;
  width: 50%;
  height: 100vh;
  background: url('./cellPhones_larg.png') top right no-repeat;
  background-size: contain;
  @media screen and (max-width: 768px) {
    display: none;
  }
`;

export const TopSection = styled.section`
  width: 100%;
  max-width: 1577px;
  margin: 0 auto;
  min-height: calc(100vh - 105px);
  display: table;
`;
export const TopSectionRow = styled.section`
  display: table-row;
`;
export const RightSideBox = styled.section`
  width: 50%;
  display: table-cell;
  position: relative;
  &:before {
    content: ' ';
    width: calc(98% + 160px);
    height: calc(100% - 50px);
    border-radius: 65px 0 0 65px;
    background: url('./mainPage.png') top right no-repeat;
    position: absolute;
    right: 0;
    top: 0;
    z-index: 2;
  }
  &:after {
    z-index: 1;
    content: ' ';
    width: calc(100% + 160px);
    height: calc(100% - 40px);
    border-radius: 65px 0 0 65px;
    background: url('./mainPageBackground.png') top right no-repeat;
    position: absolute;
    right: 30px;
    top: 40px;
  }
  @media screen and (max-width: 768px) {
    width: 100%;
    &:before {
      width: 98%;
    }
    &:after {
      width: 100%;
      right: 0;
      top: 40px;
    }
  }
`;
export const LeftSideBox = styled.section`
  width: 50%;
  display: table-cell;
  text-align: center;
  vertical-align: middle;
  z-index: 3;
  position: relative;
  @media screen and (max-width: 768px) {
    display: none;
  }
`;
export const TextContainer = styled.div`
  z-index: 3;
  width: 100%;
  min-height: calc(100vh - 155px);
  position: relative;
  padding: 25px 35px;
  display: flex;
  justify-content: space-between;
  align-items: flex-start;
  flex-direction: column;
  @media screen and (max-width: 768px) {
    padding: 25px 9px;
  }
`;
export const SmallTitle = styled.div`
  font-size: 14px;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.5;
  letter-spacing: normal;
  text-align: right;
  color: #e5f0ff;
  margin-top: 25px;
  display: flex;
  justify-content: flex-start;
  align-items: center;
  .small-title-icon {
    margin-left: 10px;
  }
  @media screen and (max-width: 768px) {
  }
`;
export const Title = styled.div`
  margin-top: 14px;
  font-size: 24px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.56;
  letter-spacing: normal;
  text-align: right;
  color: #fff;
  width: max-content;
  @media screen and (max-width: 768px) {
  }
`;
export const Text = styled.div`
  font-size: 18px;
  width: calc(100% - 30px);
  margin-top: 30px;
  line-height: 1.5;
  text-align: right;
  color: #fff;
`;
export const ButtonContainer = styled.div`
  width: 100%;
  display: flex;
  justify-content: flex-start;
  align-items: center;
  flex-wrap: wrap;
  @media screen and (max-width: 768px) {
    display: none;
  }
`;
export const BottomText = styled.div`
  font-size: 18px;
  width: 100%;
  margin-top: 30px;
  line-height: 1.5;
  text-align: right;
  color: #fff;
  font-weight: bold;
  @media screen and (max-width: 768px) {
    display: none;
  }
`;
export const SmallDevicesImageContainer = styled.div`
  display: none;
  @media screen and (max-width: 768px) {
    display: block;
    width: 100%;
    margin: 0 auto;
    text-align: center;
  }
`;
