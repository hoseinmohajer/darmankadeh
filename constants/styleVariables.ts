const Variables = {
  colors: {
    white: '#fff',
    black: '#000',
    black_100: '#505762',
    coolGrey: '#a0a6b1',
    warmPink: '#ff5876',
    blue: '#2c6ef6',
    gray: '#515761',
    darkBlue: '#001533',
    darkBlue_100: '#142743',
    green: '#64bc26',
  },
  fontSize: {},
  fontWeight: {
    normal: '400',
    medium: '500',
    bold: '700',
  },
};

export default Variables;
