import '../styles/globals.css';
import App from 'next/app';
import type { AppProps } from 'next/app';
import MainLayout from '../components/layouts';

function MyApp({ Component, pageProps }: AppProps) {
  // todo: handle multiple layouts
  // let Layout = Component?.Layout || MainLayout;
  return (
    <MainLayout>
      <Component {...pageProps} />
    </MainLayout>
  );
}

MyApp.getInitialProps = async (appContext) => {
  // calls page's `getInitialProps` and fills `appProps.pageProps`
  const appProps = await App.getInitialProps(appContext);

  return { ...appProps };
};

export default MyApp;
