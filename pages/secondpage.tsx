import React from 'react';
import SecondPageComponent from '../components/secondPage';

const SecondPage = (props: any) => {
  return <SecondPageComponent {...props} />;
};
export async function getServerSideProps() {
  // todo: it's better to handle all api call's with axios (its cross browser)
  const response = await fetch(
    `https://medicative.darmankade.com/v2/MainSearch`,
    {
      method: 'post',
      body: JSON.stringify({
        EnglishDegreeName: 'orthopedist',
        EnglishZoneName: 'tehran',
        TagId: 0,
      }),
    }
  );
  const data = await response.json();
  return {
    props: {
      data: data && data.msg ? data.msg : '',
      loading: false,
    },
  };
}
export default SecondPage;
