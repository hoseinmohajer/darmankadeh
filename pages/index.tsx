import type { NextPage } from 'next';
import MainPage from '../components/mainPage';

const Home: NextPage = () => {
  return <MainPage />;
};

export default Home;
